﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using RLWEiRS;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestPolyMod
    {
        [TestMethod]
        public void TestConstructor1()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(new int[] { 3, 1, 4 }, 3, 3);
            //when

           //then
            PolynomialModulo poly = new PolynomialModulo(new int[] { 0, 1, 1 }, 3, 3);
            Assert.AreEqual(poly, poly1);
        }

        [TestMethod]
        public void TestConstructor2()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(new int[] { 0, 5, 4, 2, 1 }, 4, 3);
            //when

            //then
            PolynomialModulo poly = new PolynomialModulo(new int[] { 2 }, 4, 3);
            Assert.AreEqual(poly, poly1);
        }


        [TestMethod]
        public void TestAdd1()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(new int[] { 0, 1, 2 },3,3);
            PolynomialModulo poly2 = new PolynomialModulo(new int[] { 1, 3 },3,3);
            //when
            PolynomialModulo p = poly1 + poly2;
            //then
            PolynomialModulo poly = new PolynomialModulo(new int[] { 1, 1, 2 },3,3);
            Assert.AreEqual(poly, p);
        }

        [TestMethod]
        public void TestAdd2()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(10,10);
            PolynomialModulo poly2 = new PolynomialModulo(new int[] { 1, 3 },10,10);
            //when
            PolynomialModulo p = poly1 + poly2;
            //then
            Assert.AreEqual(poly2, p);
        }

        [TestMethod]
        public void TestAdd3()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(new int[] { 1, 4, 2,0,1 },4,4);
            PolynomialModulo poly2 = new PolynomialModulo(new int[] { 1, 3, -2 },4,4);
            //when
            PolynomialModulo p = poly1 + poly2;
            //then
            PolynomialModulo poly = new PolynomialModulo(new int[] { 1,3 },4,4);
            Assert.AreEqual(poly, p);
        }

        [TestMethod]
        public void TestMultiply1()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(new int[] { 1, 1, 2 }, 3, 2);
            PolynomialModulo poly2 = new PolynomialModulo(new int[] { 0, 2 }, 3, 2);
            //when
            PolynomialModulo p = poly1 * poly2;
            //then
            PolynomialModulo poly = new PolynomialModulo(new int[] { 0, 2, 2, 4 }, 3, 2);
            Assert.AreEqual(poly, p);
        }
        
        [TestMethod]
        public void TestMultiply2()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(new int[] { 1, 1, 2 },4,2);
            PolynomialModulo poly2 = new PolynomialModulo(new int[] { 1, 0, 1 },4,2);
            //when
            PolynomialModulo p = poly1 * poly2;
            //then
            PolynomialModulo poly = new PolynomialModulo(4,2);
            Assert.AreEqual(poly, p);
        }

        [TestMethod]
        public void TestMultiply3()
        {
            //given
            PolynomialModulo poly1 = new PolynomialModulo(new int[] { 1, 1, 2 },3,5);
            PolynomialModulo poly2 = new PolynomialModulo(new int[] { 3 },3,5);
            //when
            PolynomialModulo p = poly1 * poly2;
            //then
            PolynomialModulo poly = new PolynomialModulo(3,5);
            Assert.AreEqual(poly, p);
        }
        
    }
}
