﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using RLWEiRS;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestBinaryHelper
    {
        [TestMethod]
        public void TestIntToBinary()
        {
            //given
            int i1 = 33;
            //when
            byte[] result1 = BinaryHelper.ToBinary(i1,8);
            //then
            byte[] seq1 = { 0, 0, 1, 0, 0, 0, 0, 1 };
            for(int i = 0; i < 8; i++)
            {
                Assert.AreEqual(seq1[i], result1[i]);
            }

        }

        [TestMethod]
        public void TestBinaryToInt()
        {
            //given
            byte[] seq1 = { 0, 1, 0, 0, 0, 1, 0, 1 };
            //when
            int i1 = BinaryHelper.ToInt(seq1);
            char c1 = (char)i1;
            //then
            Assert.AreEqual(69, i1);
            Assert.AreEqual('E', c1);
        }

        [TestMethod]
        public void TestIntToBinaryToInt()
        {
            //given
            int i1 = 100;
            //when
            int i2 = BinaryHelper.ToInt(BinaryHelper.ToBinary(i1,8));
            //then
            Assert.AreEqual(i1, i2);
        }

        [TestMethod]
        public void TestBinaryToIntToBinary()
        {
            //given
            byte[] seq1 = { 1, 0, 0, 1, 0, 1, 0, 1 };
            //when
            byte[] result1 = BinaryHelper.ToBinary(BinaryHelper.ToInt(seq1), 8);
            //then
            for (int i = 0; i < 8; i++)
            {
                Assert.AreEqual(seq1[i], result1[i]);
            }

        }
    }
}
