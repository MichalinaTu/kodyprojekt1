﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using RLWEiRS;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestRLWE
    {
        [TestMethod]
        public void RLWE()
        {
            //given
            byte[] m = { 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
                         0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
                         0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1,
                         0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1 };
            //when
            RLWE crypto = new RLWEiRS.RLWE(128, Prime.GetLowerPrimeEqual1Mod2n(128));
            PolynomialModulo u, v;
            crypto.Encrypt(m, out u, out v);
            byte[] result = crypto.Deencrypt(u, v);
            //then
            for (int i = 0; i < 8; i++)
            {
                Assert.AreEqual(m[i], result[i]);
            }

        }

    }
}
