﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using RLWEiRS;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestRS
    {
        [TestMethod]
        public void RSalpha()
        {
            //given
            RS crypto = new RS(5, 3);
            //then
            PolynomialModulo g = new PolynomialModulo(new int[] { 3, 4, 1 }, 5, 4);
            int alpha = 2;
            Assert.AreEqual(alpha, crypto.alpha);
            Assert.AreEqual(g, crypto.GeneratingPolynomial);
        }

        [TestMethod]
        public void RSbyte1()
        {
            //given
            RS crypto = new RS(5, 3);
            int[] message = { 1, 1 };
            //when
            int[] message2 = crypto.Encrypt(message);
            int[] message3 = crypto.DeencryptWithoutMistakes(message2);
            //then
            int[] message4 = { 2, 3, 0, 4 };
            for (int i = 0; i < message2.Length; i++)
            {
                Assert.AreEqual(message4[i], message2[i]);
            }
            for(int i = 0; i < message.Length; i++)
            {
                Assert.AreEqual(message[i], message3[i]);
            }
            for (int i = message.Length; i < message3.Length; i++)
            {
                Assert.AreEqual(0, message3[i]);
            }            
        }

        [TestMethod]
        public void RSbyte2()
        {
            //given
            RS crypto = new RS(5, 3);
            int[] message = { 1, 1 };
            //when
            int[] message2 = crypto.Encrypt(message);
            int[] message3 = crypto.DeencryptWithMistakes(message2);
            //then
            Assert.AreEqual(2, message3.Length);
            for (int i = 0; i < message.Length; i++)
            {
                Assert.AreEqual(message[i], message3[i]);
            }
        }

        [TestMethod]
        public void RSbyte3()
        {
            //given
            RS crypto = new RS(11, 3);
            int[] message = { 1, 9, 8, 1, 4, 6, 10, 3 };
            //when
            int[] message2 = crypto.Encrypt(message);
            int[] message3 = crypto.DeencryptWithMistakes(message2);
            //then
            Assert.AreEqual(8, message3.Length);
            for (int i = 0; i < message.Length; i++)
            {
                Assert.AreEqual(message[i], message3[i]);
            }
        }

        [TestMethod]
        public void RSbyte4()
        {
            //given
            RS crypto = new RS(13, 3);
            int[] message = { 1, 9, 8, 1, 4, 6, 10, 3, 11, 7 };
            //when
            int[] message2 = crypto.Encrypt(message);
            int[] message3 = crypto.DeencryptWithMistakes(message2);
            //then
            Assert.AreEqual(10, message3.Length);
            for (int i = 0; i < message.Length; i++)
            {
                Assert.AreEqual(message[i], message3[i]);
            }
        }

        [TestMethod]
        public void RSCombinations()
        {
            //given
            int n = 3;
            int k = 2;
            //when
            int[][] combs = RS.Combinations(n, k);
            //then
            Assert.AreEqual(0, combs[0][0]);
            Assert.AreEqual(1, combs[0][1]);
            Assert.AreEqual(0, combs[1][0]);
            Assert.AreEqual(2, combs[1][1]);
            Assert.AreEqual(1, combs[2][0]);
            Assert.AreEqual(2, combs[2][1]);
        }

        [TestMethod]
        public void RSLetterEncryptDeencrypt()
        {
            //given
            RS crypto = new RS(5, 3);
            string text = "AAAAAAAA";
            //when
            int[][] message = StringHelper.GetSequences(text, crypto.k);
            int[][] message2 = crypto.Encrypt(message);
            string text2 = StringHelper.GetText(message2);
            int[][] message22 = StringHelper.GetSequences(text2, crypto.n);
            int[][] message3 = crypto.Deencrypt(message22);
            string text3 = StringHelper.GetText(message3);
            //then
            Assert.AreEqual(text, text3);
        }

        [TestMethod]
        public void RSLetterEncryptDeencrypt2()
        {
            //given
            RS crypto = new RS(7, 3);
            string text = "ABCABCAB";
            //when
            int[][] message = StringHelper.GetSequences(text, crypto.k);
            int[][] message2 = crypto.Encrypt(message);
            string text2 = StringHelper.GetText(message2);
            int[][] message22 = StringHelper.GetSequences(text2, crypto.n);
            int[][] message3 = crypto.Deencrypt(message22);
            string text3 = StringHelper.GetText(message3);
            //then
            Assert.AreEqual(text, text3);
        }

        [TestMethod]
        public void RSLetterEncryptDeencrypt3()
        {
            //given
            RS crypto = new RS(13, 3);
            string text = "ABCABCABDELKJHGFDBCA";
            //when
            int[][] message = StringHelper.GetSequences(text, crypto.k);
            int[][] message2 = crypto.Encrypt(message);
            string text2 = StringHelper.GetText(message2);
            int[][] message22 = StringHelper.GetSequences(text2, crypto.n);
            int[][] message3 = crypto.Deencrypt(message22);
            string text3 = StringHelper.GetText(message3);
            //then
            Assert.AreEqual(text, text3);
        }

        [TestMethod]
        public void RSLetterEncryptDeencrypt4()
        {
            //given
            RS crypto = new RS(29, 3);
            string text = "ABCABCABDELKJHGFDBCAZXVNMP";
            //when
            int[][] message = StringHelper.GetSequences(text, crypto.k);
            int[][] message2 = crypto.Encrypt(message);
            string text2 = StringHelper.GetText(message2);
            int[][] message22 = StringHelper.GetSequences(text2, crypto.n);
            int[][] message3 = crypto.Deencrypt(message22);
            string text3 = StringHelper.GetText(message3);
            //then
            Assert.AreEqual(text, text3);
        }
    }
}
