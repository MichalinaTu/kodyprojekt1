﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using RLWEiRS;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestEquationsSystem
    {
        [TestMethod]
        public void EqSysMakeOneTest()
        {
            //given
            EquationsSystem equationsSystem = new EquationsSystem(
                new int[] { 0, 3, 3 }, 
                new int[][] { new int[] { 2, 4, 6 }, new int[] { 5, 1, 1 }, new int[] { 3, 2, 1 } }, 
                7);
            //when
            equationsSystem.MakeOne(0);
            //then
            Assert.AreEqual(1, equationsSystem.matrix[0][0]);
            Assert.AreEqual(2, equationsSystem.matrix[0][1]);
            Assert.AreEqual(3, equationsSystem.matrix[0][2]);
            Assert.AreEqual(0, equationsSystem.b[0]);
        }

        [TestMethod]
        public void EqSysMakeZerosTest()
        {
            //given
            EquationsSystem equationsSystem = new EquationsSystem(
                new int[] { 0, 3, 3 }, 
                new int[][] { new int[] { 2, 4, 6 }, new int[] { 5, 1, 1 }, new int[] { 3, 2, 1 } }, 
                7);
            //when
            equationsSystem.MakeOne(0);
            equationsSystem.MakeZeros(0);
            //then
            Assert.AreEqual(0, equationsSystem.matrix[1][0]);
            Assert.AreEqual(5, equationsSystem.matrix[1][1]);
            Assert.AreEqual(0, equationsSystem.matrix[1][2]);
            Assert.AreEqual(3, equationsSystem.b[1]);
            Assert.AreEqual(0, equationsSystem.matrix[2][0]);
            Assert.AreEqual(3, equationsSystem.matrix[2][1]);
            Assert.AreEqual(6, equationsSystem.matrix[2][2]);
            Assert.AreEqual(3, equationsSystem.b[2]);
        }

        [TestMethod]
        public void EqSysResolveTest()
        {
            //given
            EquationsSystem equationsSystem = new EquationsSystem(
                new int[] { 0, 3, 3 }, 
                new int[][] { new int[] { 2, 4, 6 }, new int[] { 5, 1, 1 }, new int[] { 3, 2, 1 } }, 
                7);
            //when
            int[] res = equationsSystem.Resolve();
            //then
            Assert.AreEqual(1, res[0]);
            Assert.AreEqual(2, res[1]);
            Assert.AreEqual(3, res[2]);
        }

        [TestMethod]
        public void EqSysResolveTest2()
        {
            //given
            int[] x = { 1, 9, 8, 1, 4, 6, 10, 3 };
            int q = 11, n = 10, k=8;
            int[][] matrix = new int[n][];
            int alfi = 1;
            for (int i = 0; i < n; i++)
            {
                int alfk = 1;
                matrix[i] = new int[k];
                for (int j = 0; j < k; j++)
                {
                    matrix[i][j] = alfk;
                    alfk = (alfk * alfi) % q;
                }
                alfi = (alfi * 2) % q;
            }

            int[] values = new int[n];
            for(int i=0;i<n; i++)
            {
                for(int j = 0; j < k; j++)
                {
                    values[i] = (values[i] + matrix[i][j] * x[j]) % q;
                }
                if (values[i] < 0) values[i] += q;
            }

            //when
            EquationsSystem equationsSystem = new EquationsSystem(values, matrix, q, k, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            int[] result = equationsSystem.Resolve();

            //then
            for (int i = 0; i < k; i++)
                Assert.AreEqual(x[i], result[i]);
        }

        [TestMethod]
        public void EqSysResolveTest3()
        {
            //given
            int[] x = { 1, 9, 8, 1, 4, 6, 10, 3 };
            int q = 11, n = 10, k = 8;
            int[][] matrix = new int[n][];
            int alfi = 1;
            for (int i = 0; i < n; i++)
            {
                int alfk = 1;
                matrix[i] = new int[k];
                for (int j = 0; j < k; j++)
                {
                    matrix[i][j] = alfk;
                    alfk = (alfk * alfi) % q;
                }
                alfi = (alfi * 2) % q;
            }

            PolynomialModulo poly;
            int[] values = new int[n];
            for (int i = 0; i < n; i++)
            {
                poly = new PolynomialModulo(x, q, n);
                values[i] = poly.ValueModulo(matrix[i][1]);
            }

            //when
            EquationsSystem equationsSystem = new EquationsSystem(values, matrix, q, k, new int[] { 1, 2, 3, 4, 5, 6, 7, 8 });
            int[] result = equationsSystem.Resolve();

            //then
            for (int i = 0; i < k; i++)
                Assert.AreEqual(x[i], result[i]);
        }
    }
}
