using Microsoft.VisualStudio.TestTools.UnitTesting;

using RLWEiRS;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestStringHelper
    {
        [TestMethod]
        public void TestCharToBinarySequence1()
        {
            //given
            char a = 'A';
            char b = 'B';
            char q = 'Q';
            char z = 'Z';
            //when
            char a1 = StringHelper.GetChar(StringHelper.GetBinarySequence(a));
            char b1 = StringHelper.GetChar(StringHelper.GetBinarySequence(b));
            char q1 = StringHelper.GetChar(StringHelper.GetBinarySequence(q));
            char z1 = StringHelper.GetChar(StringHelper.GetBinarySequence(z));
            //then
            Assert.AreEqual(a, a1);
            Assert.AreEqual(b, b1);
            Assert.AreEqual(q, q1);
            Assert.AreEqual(z, z1);
        }

        [TestMethod]
        public void TestCharToBinarySequence2()
        {
            //given
            byte[] s1 = { 0, 0, 0, 0, 0, 0, 0, 0 };
            byte[] s2 = { 0, 0, 0, 1, 1, 1, 0, 0 };
            byte[] s3 = { 1, 0, 0, 0, 1, 1, 1, 1 };
            byte[] s4 = { 1, 0, 0, 1, 0, 0, 0, 1 };
            //when
            byte[] ss1 = StringHelper.GetBinarySequence(StringHelper.GetChar(s1));
            byte[] ss2 = StringHelper.GetBinarySequence(StringHelper.GetChar(s2));
            byte[] ss3 = StringHelper.GetBinarySequence(StringHelper.GetChar(s3));
            byte[] ss4 = StringHelper.GetBinarySequence(StringHelper.GetChar(s4));
            //then
            for(int i=0; i < s1.Length; i++)
                Assert.AreEqual(s1[i], ss1[i]);
            for (int i = 0; i < s2.Length; i++)
                Assert.AreEqual(s2[i], ss2[i]);
            for (int i = 0; i < s3.Length; i++)
                Assert.AreEqual(s3[i], ss3[i]);
            for (int i = 0; i < s4.Length; i++)
                Assert.AreEqual(s4[i], ss4[i]);
        }

        [TestMethod]
        public void TestCopy()
        {
            //given
            byte[] source = { 1, 3, 4 };
            byte[] dest = { 0, 0, 0, 0, 0, 0, 0 };
            //when
            StringHelper.Copy(dest, source, 1);
            //then
            byte[] tab = { 0, 1, 3, 4, 0, 0, 0 };
            Assert.AreEqual(tab.Length, dest.Length);
            for (int i = 0; i < tab.Length; i++)
                Assert.AreEqual(tab[i], dest[i]);
        }

        [TestMethod]
        public void TestGetText()
        {
            //given
            string text = "ABCD";
            //when
            byte[][] code = StringHelper.GetBinarySequences(text, 32);
            string result = StringHelper.GetText(code);
            //then
            Assert.AreEqual(text, result);
        }

        [TestMethod]
        public void TestGetText2()
        {
            //given
            string text = "RGNUKA?.";
            //when
            byte[][] code = StringHelper.GetBinarySequences(text, 64);
            string result = StringHelper.GetText(code);
            //then
            Assert.AreEqual(text, result);
        }

        [TestMethod]
        public void TestNormaliseLength()
        {
            //given
            string text = "RGNUKA?.";
            int n = 64;
            //when
            string result = StringHelper.NormaliseTextLength(text,n);
            //then
            Assert.AreEqual(0, result.Length % (n / 8));
            Assert.AreEqual(text, result);
        }

        [TestMethod]
        public void TestNormaliseLength2()
        {
            //given
            string text = "aaaaaaaaannnjhnikjnklk";
            int n = 128;
            //when
            string result = StringHelper.NormaliseTextLength(text, n);
            //then
            Assert.AreEqual(0, result.Length % (n / 8));
        }

        [TestMethod]
        public void TestTextToIntToText()
        {
            //given
            string text = "RGNUKAL";
            int n = 2;
            //when
            int[][] seq = StringHelper.GetSequences(text, n);
            string text2 = StringHelper.GetText(seq);
            //then
            for(int i = 0; i < text.Length; i++)
            {
                Assert.AreEqual(text[i], text2[i]);
            }
        }

        [TestMethod]
        public void TestIntToTextToInt()
        {
            //given
            int[][] seq = new int[][] { new int[] { 1, 2 }, new int[] { 3, 4 }, new int[] { 5, 6 } };
            int n = 2;
            //when
            string text = StringHelper.GetText(seq);
            int[][] seq2 = StringHelper.GetSequences(text, 2);
            //then
            for (int i = 0; i < 3; i++)
            {
                Assert.AreEqual(seq[i][0], seq2[i][0]);
                Assert.AreEqual(seq[i][1], seq2[i][1]);
            }
        }
    }
}
