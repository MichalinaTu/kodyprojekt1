﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using RLWEiRS;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTestPolynomial
    {
        [TestMethod]
        public void TestAdd1()
        {
            //given
            Polynomial poly1 = new Polynomial(new int[] { 0, 1, 2 });
            Polynomial poly2 = new Polynomial(new int[] { 1, 3 });
            //when
            Polynomial p = poly1 + poly2;
            //then
            Polynomial poly = new Polynomial(new int[] { 1, 4, 2 });
            Assert.AreEqual(poly, p);
        }

        [TestMethod]
        public void TestAdd2()
        {
            //given
            Polynomial poly1 = new Polynomial();
            Polynomial poly2 = new Polynomial(new int[] { 1, 3 });
            //when
            Polynomial p = poly1 + poly2;
            //then
            Assert.AreEqual(poly2, p);
        }

        [TestMethod]
        public void TestAdd3()
        {
            //given
            Polynomial poly1 = new Polynomial(new int[] { 1, 4, 2 });
            Polynomial poly2 = new Polynomial(new int[] { 1, 3, -2 });
            //when
            Polynomial p = poly1 + poly2;
            //then
            Polynomial poly = new Polynomial(new int[] { 2, 7 });
            Assert.AreEqual(poly, p);
        }

        [TestMethod]
        public void TestMultiply1()
        {
            //given
            Polynomial poly1 = new Polynomial(new int[] { 1, 1, 2 });
            Polynomial poly2 = new Polynomial(new int[] { 0, 2 });
            //when
            Polynomial p = poly1 * poly2;
            //then
            Polynomial poly = new Polynomial(new int[] { 0, 2, 2, 4 });
            Assert.AreEqual(poly, p);
        }

        [TestMethod]
        public void TestMultiply2()
        {
            //given
            Polynomial poly1 = new Polynomial(new int[] { 1, 1, 2 });
            Polynomial poly2 = new Polynomial();
            //when
            Polynomial p = poly1 * poly2;
            //then
            Polynomial poly = new Polynomial();
            Assert.AreEqual(poly, p);
        }

        [TestMethod]
        public void TestMultiply3()
        {
            //given
            Polynomial poly1 = new Polynomial(new int[] { 1, 1, 2 });
            Polynomial poly2 = new Polynomial(new int[] { 3 });
            //when
            Polynomial p = poly1 * poly2;
            //then
            Polynomial poly = new Polynomial(new int[] { 3, 3, 6 });
            Assert.AreEqual(poly, p);
        }

    }
}
