﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public class PolynomialModulo
    {
        private int _n;
        private int _q;
        private int[] coefficients;
        private int degree;

        public int Degree { get { return degree; } }
        public int n { get { return _n; } }
        public int q { get { return _q; } }
        public int this[int p]
        {
            get
            {
                if (p > degree)
                    return 0;
                return coefficients[p];
            }
        }


        public PolynomialModulo(int q, int n)
        {
            this._q = q;
            this._n = n;
            _n = n;
            degree = 0;
            coefficients = new int[] { 0 };
        }


        public PolynomialModulo(int[] coefficients, int q, int n)
        {
            this._q = q;
            this._n = n;
            degree = coefficients.Length - 1;
            while (degree > 0 && coefficients[degree] % q == 0)
            {
                degree--;
            }
            while (degree >= n)
            {
                int a = coefficients[degree];
                coefficients[degree] -= a;
                coefficients[degree - n] -= a;
                degree--;
            }
            while (degree > 0 && coefficients[degree] % q == 0)
            {
                degree--;
            }
            this.coefficients = Copy(coefficients, degree + 1, q);
        }


        public PolynomialModulo(byte[] coefficients, int q, int n)
        {
            int[] coef = Copy(coefficients, q);
            this._q = q;
            this._n = n;
            degree = coefficients.Length - 1;
            while (degree > 0 && coef[degree] % q == 0)
            {
                degree--;
            }
            while (degree >= n)
            {
                int a = coef[degree];
                coef[degree] -= a;
                coef[degree - n] -= a;
                degree--;
            }
            while (degree > 0 && coef[degree] % q == 0)
            {
                degree--;
            }
            this.coefficients = Copy(coef, degree + 1, q);
        }


        public PolynomialModulo(int degree, int q, int n)
        {
            this._n = n;
            this._q = q;
            this.degree = degree;
            coefficients = new int[degree + 1];
            coefficients[degree] = 1;
            while (this.degree >= n)
            {
                int a = coefficients[this.degree];
                coefficients[this.degree] -= a;
                coefficients[this.degree - n] -= a;
                this.degree--;
            }
            while (degree > 0 && coefficients[degree] % q == 0)
            {
                degree--;
            }
            if (this.degree >= n)
                coefficients = Copy(coefficients, this.degree + 1);
        }


        public static PolynomialModulo RandomPolynomial(int degree, int max_value, int q, int n)
        {
            Random random = new Random();
            PolynomialModulo poly = new PolynomialModulo(degree, q, n);
            for (int i = 0; i < poly.coefficients.Length; i++)
                poly.coefficients[i] = (random.Next(-max_value, max_value + 1) + q) % q;
            return poly;
        }


        public PolynomialModulo Copy()
        {
            return new PolynomialModulo(coefficients,_q,_n);
        }


        public override string ToString()
        {
            string[] stringTab = new string[coefficients.Length];
            stringTab[0] = String.Format("{0}", coefficients[0]);
            for (int i = 1; i <= degree; i++)
            {
                stringTab[i] = String.Format(" + {0}*x^{1}", coefficients[i], i);
            }
            return String.Concat(stringTab);
        }


        public string GetCoefficientsString()
        {
            string[] stringTab = new string[coefficients.Length];
            stringTab[0] = String.Format("{0}", coefficients[0]);
            for (int i = 1; i <= degree; i++)
            {
                stringTab[i] = String.Format(", {0}", coefficients[i]);
            }
            return String.Concat(stringTab);
        }


        public override bool Equals(object obj)
        {
            if (!(obj is PolynomialModulo))
                return false;
            if (((PolynomialModulo)obj).degree != this.degree)
                return false;
            if (((PolynomialModulo)obj)._q != this._q)
                return false;
            if (((PolynomialModulo)obj)._n != this._n)
                return false;
            for (int i = 0; i <= degree; i++)
                if (((PolynomialModulo)obj).coefficients[i] != this.coefficients[i])
                    return false;
            return true;
        }


        public void Multiply(int a)
        {
            for (int i = 0; i <= degree; i++)
                coefficients[i] = a * coefficients[i];
            MakeModulo(coefficients);
        }


        public int Value(int x)
        {
            int value = coefficients[0];
            int x_i = 1;
            for (int i = 1; i <= degree; i++)
            {
                x_i *= x;
                value += coefficients[i] * x_i;
            }
            return value;
        }

        public int ValueModulo(int x)
        {
            int value = coefficients[0];
            int x_i = 1;
            for (int i = 1; i <= degree; i++)
            {
                x_i = (x_i * x)%_q;
                value = (value + coefficients[i] * x_i)%_q;
            }
            if (value < 0) value += _q;
            return value;
        }

        static public PolynomialModulo operator *(PolynomialModulo w1, PolynomialModulo w2)
        {
            if (w1._q != w2._q || w1._n != w2._n)
                throw new ArgumentException("Mnożenie może być wykonane tylko w tym samym pieścieniu");
            int st = w1.degree + w2.degree;
            int[] tab = new int[st + 1];
            for (int i1 = 0; i1 <= w1.degree; i1++)
                for (int i2 = 0; i2 <= w2.degree; i2++)
                {
                    tab[i1 + i2] = (tab[i1 + i2] + w1.coefficients[i1] * w2.coefficients[i2]) % w1._q;
                }
            return new PolynomialModulo(tab, w1._q, w1._n);
        }
        static public PolynomialModulo operator *(PolynomialModulo w, int s)
        {
            PolynomialModulo poly = w.Copy();
            poly.Multiply(s);
            return poly;
        }
        static public PolynomialModulo operator *(int s, PolynomialModulo w)
        {
            PolynomialModulo poly = w.Copy();
            poly.Multiply(s);
            return poly;
        }

        public static PolynomialModulo operator +(PolynomialModulo w1, PolynomialModulo w2)
        {
            PolynomialModulo w;
            if (w1._q != w2._q || w1._n != w2._n)
                throw new ArgumentException("Dodawanie może być wykonane tylko w tym samym pieścieniu");
            if (w1.degree < w2.degree)
            {
                w = w1;
                w1 = w2;
                w2 = w;
            }
            //w1.st>=w2.st
            int[] tab = Copy(w1.coefficients,w1._q);
            for (int i = 0; i < w2.coefficients.Length; i++)
                tab[i] = (tab[i] + w2.coefficients[i]) % w1._q;
            w = new PolynomialModulo(tab, w1._q, w1._n);
            return w;
        }

        public static PolynomialModulo operator -(PolynomialModulo w1, PolynomialModulo w2)
        {
            return w1 +(w2*(-1));
        }

        static public PolynomialModulo Sum(List<PolynomialModulo> lista)
        {
            int st = 0;
            if (lista == null || lista.Count == 0)
                throw new ArgumentException("Lista jest pusta");
            int q = lista[0]._q;
            int n = lista[0]._n;
            foreach (var w in lista)
            {
                if (w._q != q || w._n != n)
                    throw new ArgumentException("Dodawanie może być wykonane tylko w tym samym pieścieniu");
                if (st < w.degree)
                    st = w.degree;
            }        
            int[] tab = new int[st + 1];
            foreach (var w in lista)
                for (int i = 0; i <= w.degree; i++)
                    tab[i] += w.coefficients[i];
            return new PolynomialModulo(tab, q, n);
        }
        
        private static int[] Copy(int[] tab, int q_mod)
        {
            int[] copy = new int[tab.Length];
            for (int i = 0; i < tab.Length; i++)
                copy[i] = tab[i];
            MakeModulo(copy,q_mod);
            return copy;
        }

        private static int[] Copy(byte[] tab, int q_mod)
        {
            int[] copy = new int[tab.Length];
            for (int i = 0; i < tab.Length; i++)
                copy[i] = tab[i];
            MakeModulo(copy, q_mod);
            return copy;
        }

        private static int[] Copy(int[] tab, int n, int q_mod)
        {
            int[] copy = new int[n];
            for (int i = 0; i < tab.Length && i < n; i++)
                copy[i] = tab[i];
            MakeModulo(copy, q_mod);
            return copy;
        }

        private static void MakeModulo (int[] tab, int q_mod)
        {
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = tab[i] % q_mod;
                if (tab[i] < 0)
                    tab[i] = tab[i] + q_mod;
            }
        }

        private void MakeModulo(int[] tab)
        {
            for (int i = 0; i < tab.Length; i++)
            {
                tab[i] = tab[i] % _q;
                if (tab[i] < 0)
                    tab[i] = tab[i] + _q;
            }
        }
    }
}
