﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    class RandomGauss
    {
        Random rand;
        int max_value;

        public RandomGauss(int max_val)
        {
            rand = new Random();
            max_value = max_val;
        }

        public int Next()
        {
            int k = rand.Next(0, max_value + 1);
            int kk = rand.Next(0, k + 1);
            return rand.Next(-kk, kk + 1);
        }
    }
}
