﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public static class BinaryHelper
    {
        /// <summary>
        /// Konwersja liczby z systemu 10(int) do systemu 2(int[n])
        /// </summary>
        public static byte[] ToBinary(int a, int n)
        {
            byte[] bin = new byte[n];
            for (int i = 0; i < n; i++)
            {
                bin[n-1-i] = (byte)(a % 2);
                a /= 2;
            }
            return bin;
        }
        /// <summary>
        /// Konwersja tablicy z systemu 2(int[]) do systemu 10(int)
        /// </summary>
        public static int ToInt(byte[] bin)
        {
            int a = 0;
            int p = 1;
            for (int i = 0; i < bin.Length; i++)
            {
                a += bin[bin.Length - i - 1] * p;
                p *= 2;
            }
            return a;
        }

    }
}
