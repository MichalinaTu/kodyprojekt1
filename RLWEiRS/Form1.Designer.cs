﻿namespace RLWEiRS
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.richTextBox_source = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_n = new System.Windows.Forms.Label();
            this.label_publicKey = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown_k = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.button_RLWEdecrypt = new System.Windows.Forms.Button();
            this.button_RLWEencrypt = new System.Windows.Forms.Button();
            this.button_generate_private_key = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label_privateKey = new System.Windows.Forms.Label();
            this.button_insert_n = new System.Windows.Forms.Button();
            this.button_generate_public_key = new System.Windows.Forms.Button();
            this.label_q = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.button_RSdeencrypt = new System.Windows.Forms.Button();
            this.button_RSencrypt = new System.Windows.Forms.Button();
            this.label_alfa = new System.Windows.Forms.Label();
            this.label_gen_poly = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.numericUpDown_delta = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.button_insert_delta = new System.Windows.Forms.Button();
            this.numericUpDown_RSq = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button_insert_q = new System.Windows.Forms.Button();
            this.label_delta = new System.Windows.Forms.Label();
            this.label_qRS = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.richTextBox_dest = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_k)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_delta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RSq)).BeginInit();
            this.SuspendLayout();
            // 
            // richTextBox_source
            // 
            this.richTextBox_source.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.richTextBox_source.Location = new System.Drawing.Point(9, 25);
            this.richTextBox_source.Name = "richTextBox_source";
            this.richTextBox_source.Size = new System.Drawing.Size(308, 194);
            this.richTextBox_source.TabIndex = 0;
            this.richTextBox_source.Text = "";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tekst źródłowy:";
            // 
            // label_n
            // 
            this.label_n.AutoSize = true;
            this.label_n.Location = new System.Drawing.Point(6, 16);
            this.label_n.Name = "label_n";
            this.label_n.Size = new System.Drawing.Size(55, 13);
            this.label_n.TabIndex = 2;
            this.label_n.Text = "n=2^4=16";
            // 
            // label_publicKey
            // 
            this.label_publicKey.AutoSize = true;
            this.label_publicKey.Location = new System.Drawing.Point(6, 146);
            this.label_publicKey.Name = "label_publicKey";
            this.label_publicKey.Size = new System.Drawing.Size(22, 13);
            this.label_publicKey.TabIndex = 3;
            this.label_publicKey.Text = "( ; )";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "klucz publiczny:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.numericUpDown_k);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.button_RLWEdecrypt);
            this.groupBox1.Controls.Add(this.button_RLWEencrypt);
            this.groupBox1.Controls.Add(this.button_generate_private_key);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label_privateKey);
            this.groupBox1.Controls.Add(this.button_insert_n);
            this.groupBox1.Controls.Add(this.button_generate_public_key);
            this.groupBox1.Controls.Add(this.label_q);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label_publicKey);
            this.groupBox1.Controls.Add(this.label_n);
            this.groupBox1.Location = new System.Drawing.Point(12, 232);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(623, 219);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RLWE";
            // 
            // numericUpDown_k
            // 
            this.numericUpDown_k.Location = new System.Drawing.Point(185, 14);
            this.numericUpDown_k.Minimum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.numericUpDown_k.Name = "numericUpDown_k";
            this.numericUpDown_k.Size = new System.Drawing.Size(47, 20);
            this.numericUpDown_k.TabIndex = 19;
            this.numericUpDown_k.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(167, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(19, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "k=";
            // 
            // button_RLWEdecrypt
            // 
            this.button_RLWEdecrypt.Location = new System.Drawing.Point(124, 177);
            this.button_RLWEdecrypt.Name = "button_RLWEdecrypt";
            this.button_RLWEdecrypt.Size = new System.Drawing.Size(95, 23);
            this.button_RLWEdecrypt.TabIndex = 17;
            this.button_RLWEdecrypt.Text = "Deszyfruj";
            this.button_RLWEdecrypt.UseVisualStyleBackColor = true;
            this.button_RLWEdecrypt.Click += new System.EventHandler(this.button_decrypt_Click);
            // 
            // button_RLWEencrypt
            // 
            this.button_RLWEencrypt.Location = new System.Drawing.Point(9, 177);
            this.button_RLWEencrypt.Name = "button_RLWEencrypt";
            this.button_RLWEencrypt.Size = new System.Drawing.Size(95, 23);
            this.button_RLWEencrypt.TabIndex = 16;
            this.button_RLWEencrypt.Text = "Szyfruj";
            this.button_RLWEencrypt.UseVisualStyleBackColor = true;
            this.button_RLWEencrypt.Click += new System.EventHandler(this.button_encrypt_Click);
            // 
            // button_generate_private_key
            // 
            this.button_generate_private_key.Location = new System.Drawing.Point(9, 49);
            this.button_generate_private_key.Name = "button_generate_private_key";
            this.button_generate_private_key.Size = new System.Drawing.Size(196, 23);
            this.button_generate_private_key.TabIndex = 15;
            this.button_generate_private_key.Text = "Generuj kryprosystem i klucz prywatny";
            this.button_generate_private_key.UseVisualStyleBackColor = true;
            this.button_generate_private_key.Click += new System.EventHandler(this.button_generate_private_key_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "klucz prywatny:";
            // 
            // label_privateKey
            // 
            this.label_privateKey.AutoSize = true;
            this.label_privateKey.Location = new System.Drawing.Point(6, 88);
            this.label_privateKey.Name = "label_privateKey";
            this.label_privateKey.Size = new System.Drawing.Size(16, 13);
            this.label_privateKey.TabIndex = 12;
            this.label_privateKey.Text = "( )";
            // 
            // button_insert_n
            // 
            this.button_insert_n.Location = new System.Drawing.Point(238, 11);
            this.button_insert_n.Name = "button_insert_n";
            this.button_insert_n.Size = new System.Drawing.Size(67, 23);
            this.button_insert_n.TabIndex = 11;
            this.button_insert_n.Text = "Wstaw n";
            this.button_insert_n.UseVisualStyleBackColor = true;
            this.button_insert_n.Click += new System.EventHandler(this.button_insert_n_Click);
            // 
            // button_generate_public_key
            // 
            this.button_generate_public_key.Location = new System.Drawing.Point(9, 107);
            this.button_generate_public_key.Name = "button_generate_public_key";
            this.button_generate_public_key.Size = new System.Drawing.Size(132, 23);
            this.button_generate_public_key.TabIndex = 10;
            this.button_generate_public_key.Text = "Generuj klucz publiczny";
            this.button_generate_public_key.UseVisualStyleBackColor = true;
            this.button_generate_public_key.Click += new System.EventHandler(this.button_generate_public_key_Click);
            // 
            // label_q
            // 
            this.label_q.AutoSize = true;
            this.label_q.Location = new System.Drawing.Point(6, 29);
            this.label_q.Name = "label_q";
            this.label_q.Size = new System.Drawing.Size(31, 13);
            this.label_q.TabIndex = 5;
            this.label_q.Text = "q=97";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.button_RSdeencrypt);
            this.groupBox4.Controls.Add(this.button_RSencrypt);
            this.groupBox4.Controls.Add(this.label_alfa);
            this.groupBox4.Controls.Add(this.label_gen_poly);
            this.groupBox4.Controls.Add(this.button1);
            this.groupBox4.Controls.Add(this.numericUpDown_delta);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.button_insert_delta);
            this.groupBox4.Controls.Add(this.numericUpDown_RSq);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.button_insert_q);
            this.groupBox4.Controls.Add(this.label_delta);
            this.groupBox4.Controls.Add(this.label_qRS);
            this.groupBox4.Location = new System.Drawing.Point(12, 457);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(623, 189);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "RS";
            // 
            // button_RSdeencrypt
            // 
            this.button_RSdeencrypt.Location = new System.Drawing.Point(124, 156);
            this.button_RSdeencrypt.Name = "button_RSdeencrypt";
            this.button_RSdeencrypt.Size = new System.Drawing.Size(95, 23);
            this.button_RSdeencrypt.TabIndex = 30;
            this.button_RSdeencrypt.Text = "Deszyfruj";
            this.button_RSdeencrypt.UseVisualStyleBackColor = true;
            this.button_RSdeencrypt.Click += new System.EventHandler(this.button_RSdeencrypt_Click);
            // 
            // button_RSencrypt
            // 
            this.button_RSencrypt.Location = new System.Drawing.Point(9, 156);
            this.button_RSencrypt.Name = "button_RSencrypt";
            this.button_RSencrypt.Size = new System.Drawing.Size(95, 23);
            this.button_RSencrypt.TabIndex = 29;
            this.button_RSencrypt.Text = "Szyfruj";
            this.button_RSencrypt.UseVisualStyleBackColor = true;
            this.button_RSencrypt.Click += new System.EventHandler(this.button_RSencrypt_Click);
            // 
            // label_alfa
            // 
            this.label_alfa.AutoSize = true;
            this.label_alfa.Location = new System.Drawing.Point(3, 85);
            this.label_alfa.Name = "label_alfa";
            this.label_alfa.Size = new System.Drawing.Size(30, 13);
            this.label_alfa.TabIndex = 28;
            this.label_alfa.Text = "alfa=";
            // 
            // label_gen_poly
            // 
            this.label_gen_poly.AutoSize = true;
            this.label_gen_poly.Location = new System.Drawing.Point(3, 98);
            this.label_gen_poly.Name = "label_gen_poly";
            this.label_gen_poly.Size = new System.Drawing.Size(123, 13);
            this.label_gen_poly.TabIndex = 27;
            this.label_gen_poly.Text = "wielomian generujący: ( )";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 59);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(273, 23);
            this.button1.TabIndex = 26;
            this.button1.Text = "Generuj kod i wyznacz alfa oraz wielomian generujący";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericUpDown_delta
            // 
            this.numericUpDown_delta.Location = new System.Drawing.Point(363, 24);
            this.numericUpDown_delta.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_delta.Name = "numericUpDown_delta";
            this.numericUpDown_delta.Size = new System.Drawing.Size(47, 20);
            this.numericUpDown_delta.TabIndex = 25;
            this.numericUpDown_delta.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(328, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 24;
            this.label5.Text = "delta=";
            // 
            // button_insert_delta
            // 
            this.button_insert_delta.Location = new System.Drawing.Point(416, 21);
            this.button_insert_delta.Name = "button_insert_delta";
            this.button_insert_delta.Size = new System.Drawing.Size(75, 23);
            this.button_insert_delta.TabIndex = 23;
            this.button_insert_delta.Text = "Wstaw deltę";
            this.button_insert_delta.UseVisualStyleBackColor = true;
            this.button_insert_delta.Click += new System.EventHandler(this.button_insert_delta_Click);
            // 
            // numericUpDown_RSq
            // 
            this.numericUpDown_RSq.Location = new System.Drawing.Point(171, 24);
            this.numericUpDown_RSq.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown_RSq.Name = "numericUpDown_RSq";
            this.numericUpDown_RSq.Size = new System.Drawing.Size(47, 20);
            this.numericUpDown_RSq.TabIndex = 22;
            this.numericUpDown_RSq.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(153, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(19, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "q=";
            // 
            // button_insert_q
            // 
            this.button_insert_q.Location = new System.Drawing.Point(224, 21);
            this.button_insert_q.Name = "button_insert_q";
            this.button_insert_q.Size = new System.Drawing.Size(67, 23);
            this.button_insert_q.TabIndex = 20;
            this.button_insert_q.Text = "Wstaw q";
            this.button_insert_q.UseVisualStyleBackColor = true;
            this.button_insert_q.Click += new System.EventHandler(this.button_insert_q_Click);
            // 
            // label_delta
            // 
            this.label_delta.AutoSize = true;
            this.label_delta.Location = new System.Drawing.Point(6, 31);
            this.label_delta.Name = "label_delta";
            this.label_delta.Size = new System.Drawing.Size(42, 13);
            this.label_delta.TabIndex = 4;
            this.label_delta.Text = "delta=3";
            // 
            // label_qRS
            // 
            this.label_qRS.AutoSize = true;
            this.label_qRS.Location = new System.Drawing.Point(6, 16);
            this.label_qRS.Name = "label_qRS";
            this.label_qRS.Size = new System.Drawing.Size(25, 13);
            this.label_qRS.TabIndex = 3;
            this.label_qRS.Text = "q=5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(327, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Tekst wynikowy:";
            // 
            // richTextBox_dest
            // 
            this.richTextBox_dest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBox_dest.Location = new System.Drawing.Point(327, 25);
            this.richTextBox_dest.Name = "richTextBox_dest";
            this.richTextBox_dest.Size = new System.Drawing.Size(308, 194);
            this.richTextBox_dest.TabIndex = 9;
            this.richTextBox_dest.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(650, 656);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.richTextBox_dest);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.richTextBox_source);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_k)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_delta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RSq)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox richTextBox_source;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_n;
        private System.Windows.Forms.Label label_publicKey;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button_RLWEdecrypt;
        private System.Windows.Forms.Button button_RLWEencrypt;
        private System.Windows.Forms.Button button_generate_private_key;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_privateKey;
        private System.Windows.Forms.Button button_insert_n;
        private System.Windows.Forms.Button button_generate_public_key;
        private System.Windows.Forms.Label label_q;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox richTextBox_dest;
        private System.Windows.Forms.NumericUpDown numericUpDown_k;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_delta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button_insert_delta;
        private System.Windows.Forms.NumericUpDown numericUpDown_RSq;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_insert_q;
        private System.Windows.Forms.Label label_delta;
        private System.Windows.Forms.Label label_qRS;
        private System.Windows.Forms.Button button_RSdeencrypt;
        private System.Windows.Forms.Button button_RSencrypt;
        private System.Windows.Forms.Label label_alfa;
        private System.Windows.Forms.Label label_gen_poly;
        private System.Windows.Forms.Button button1;
    }
}

