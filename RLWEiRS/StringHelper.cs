﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public static class StringHelper
    {
        private static int bin_lenght = 8;


        public static string CleanTextToUpperLetters(string sourceText)
        {
            string tUpper = sourceText.ToUpper();
            string text = new string(tUpper.Where(c => Char.IsLetter(c)).ToArray());
            return text;
        }


        public static string[] MakeKLettersBlocks(string sourceText, int k)
        {
            int blocks = (int)Math.Ceiling(sourceText.Length / (double)k);
            string[] texts = new string[blocks];
            for (int i = 0; i < blocks; i++)
            {
                texts[i] = sourceText.Substring(k * i, Math.Min(k, sourceText.Length - k * i));
            }
            return texts;
        }


        public static string NormaliseTextLength(string sourceText, int n)
        {
            if (sourceText.Length % (n / 8) == 0)
                return sourceText;

            int nn = n / 8 - sourceText.Length % (n / 8);
            string space = "";
            while (nn > 0)
            {
                space += " ";
                nn--;
            }
            return sourceText + space;
        }

        ////////////////////// Text -> BinarySequence -> Text

        public static byte[] GetBinarySequence(char c)
        {
            int i = (int)c;
            byte[] sequence = { 0, 0, 0, 0, 0, 0, 0, 0 };
            if (i > 255)
                return sequence;
            return BinaryHelper.ToBinary(i, bin_lenght);
        }


        public static byte[][] GetBinarySequences(string text, int n)
        {
            int k = n / bin_lenght;
            int blocksNumber = (int)Math.Ceiling(text.Length / (double)k);
            byte[][] sequences = new byte[blocksNumber][];
            for (int i = 0; i < blocksNumber; i++)
            {
                int block0 = i * k;
                sequences[i] = new byte[n];
                for (int j = 0; j < k && block0 + j < text.Length; j++)
                {
                    Copy(sequences[i], GetBinarySequence(text[block0 + j]), j * bin_lenght);
                }
            }
            return sequences;
        }


        public static void Copy(byte[] dest, byte[] source, int from)
        {
            for (int i = 0; i < source.Length; i++)
            {
                dest[from + i] = source[i];
            }
        }


        public static byte[] Copy(byte[] source, int from, int length)
        {
            byte[] dest = new byte[length];
            for (int i = 0; i < length; i++)
            {
                dest[i] = source[from + i];
            }
            return dest;
        }


        public static char GetChar(byte[] seq)
        {
            return (char)BinaryHelper.ToInt(seq);
        }


        public static char GetChar(byte[] sequence, int from)
        {
            return GetChar(Copy(sequence, from, bin_lenght));
        }


        public static string GetText(byte[][] sequence)
        {
            List<char> list = new List<char>();
            foreach (var seq in sequence)
            {
                for (int i = 0; i + bin_lenght <= seq.Length; i += bin_lenght)
                {
                    char c = GetChar(seq, i);
                    list.Add(c);
                }
            }
            return new string(list.ToArray());
        }

        ////////////////////// Text -> IntSequence -> Text

        public static int[] GetSequence(string text, int k)
        {
            int[] sequence = new int[k];
            for (int i = 0; i < text.Length; i++)
            {
                sequence[i] = text[i] - 'A' + 1;
            }
            return sequence;
        }


        public static int[] GetSequence(string text)
        {
            return GetSequence(text, text.Length);
        }


        public static int[][] GetSequences(string text, int k)
        {
            string[] blocks = MakeKLettersBlocks(text, k);
            int[][] sequences = new int[blocks.Length][];
            for (int i = 0; i < blocks.Length - 1; i++)
            {
                sequences[i] = GetSequence(blocks[i]);
            }
            sequences[blocks.Length - 1] = GetSequence(blocks[blocks.Length - 1], k);
            return sequences;
        }


        public static string GetText(int[] seq)
        {
            char[] text = new char[seq.Length];
            for (int i = 0; i < text.Length; i++)
            {
                text[i] = (char)(seq[i] + 'A' - 1);
            }
            return new string(text);
        }


        public static string GetText(int[][] seq)
        {
            string[] text = new string[seq.Length];
            for (int i = 0; i < text.Length; i++)
            {
                text[i] = GetText(seq[i]);
            }
            return string.Concat(text);
        }
    }
}