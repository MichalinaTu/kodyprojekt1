﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public class Polynomial
    {
        private int[] coefficients;
        private int degree;

        public int Degree { get { return degree; } }
        public int this[int p]
        {
            get
            {
                if (p > degree)
                    return 0;
                return coefficients[p];
            }
        }

        public Polynomial()
        {
            degree = 0;
            coefficients = new int[] { 0 };
        }

        /*public Polynomial(int[] coefficients)
        {
            this.coefficients = Copy(coefficients);
            degree = coefficients.Length - 1;
        }*/

        public Polynomial(int[] coefficients)
        {
            degree = coefficients.Length - 1;
            while (degree > 0 && coefficients[degree]==0)
            {
                degree--;
            }
            this.coefficients = Copy(coefficients, degree + 1);
        }

        public Polynomial(int degree)
        {
            this.degree = degree;
            coefficients = new int[degree + 1];
            coefficients[degree] = 1;
        }
        
        public static Polynomial PolynomialWithAllCoefficientsEqual (int degree, int value)
        {
            Polynomial poly = new Polynomial(degree);
            for (int i = 0; i < poly.coefficients.Length; i++)
                poly.coefficients[i] = value;
            return poly;
        }

        public Polynomial Copy()
        {
            return new Polynomial(coefficients);
        }

        public override string ToString()
        {
            string[] stringTab = new string[coefficients.Length];
            stringTab[0] = String.Format("{0}", coefficients[0]);
            for (int i = 1; i <= degree; i++)
            {
                stringTab[i] = String.Format(" + {0}*x^{1}", coefficients[i], i);
            }
            return String.Concat(stringTab);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Polynomial))
                return false;
            if (((Polynomial)obj).degree != this.degree)
                return false;
            for (int i = 0; i <= degree; i++)
                if (((Polynomial)obj).coefficients[i] != this.coefficients[i])
                    return false;
            return true;
        }

        public void Multiply (int a)
        {
            for (int i = 0; i <= degree; i++)
                coefficients[i] = a * coefficients[i];
        }

        public int Value (int x)
        {
            int value = coefficients[0];
            int x_i = 1;
            for(int i = 1; i <= degree; i++)
            {
                x_i *= x;
                value += coefficients[i] * x_i;
            }
            return value;
        }

        static public Polynomial operator *(Polynomial w1, Polynomial w2)
        {
            int st = w1.degree + w2.degree;
            int[] tab = new int[st + 1];
            for (int i1 = 0; i1 <= w1.degree; i1++)
                for (int i2 = 0; i2 <= w2.degree; i2++)
                {
                    tab[i1 + i2] += w1.coefficients[i1] * w2.coefficients[i2];
                }
            return new Polynomial(tab);
        }
        static public Polynomial operator *(Polynomial w, int s)
        {
            Polynomial poly = w.Copy();
            poly.Multiply(s);
            return poly;
        }
        static public Polynomial operator *(int s, Polynomial w)
        {
            Polynomial poly = w.Copy();
            poly.Multiply(s);
            return poly;
        }

        public static Polynomial operator +(Polynomial w1, Polynomial w2)
        {
            Polynomial w;
            if (w1.degree < w2.degree)
            {
                w = w1;
                w1 = w2;
                w2 = w;
            }
            //w1.st>=w2.st
            int[] tab = Copy(w1.coefficients);
            for (int i = 0; i < w2.coefficients.Length; i++)
                tab[i] += w2.coefficients[i];
            w = new Polynomial(tab);
            return w;
        }

        static public Polynomial Sum(List<Polynomial> lista)
        {
            int st = 0;
            foreach (var w in lista)
                if (st < w.degree)
                    st = w.degree;
            int[] tab = new int[st + 1];
            foreach (var w in lista)
                for (int i = 0; i <= w.degree; i++)
                    tab[i] += w.coefficients[i];
            return new Polynomial(tab);
        }

        private static int[] Copy (int[] tab)
        {
            int[] copy = new int[tab.Length];
            for (int i = 0; i < tab.Length; i++)
                copy[i] = tab[i];
            return copy;
        }

        private static int[] Copy(int[] tab, int n)
        {
            int[] copy = new int[n];
            for (int i = 0; i < tab.Length && i<n; i++)
                copy[i] = tab[i];
            return copy;
        }
    }
}
