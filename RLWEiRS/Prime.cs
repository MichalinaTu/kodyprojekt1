﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public static class Prime
    {
        static public int GetLowerPrimeEqual1Mod2n (int n)
        {
            int p = 1;
            while (!IsPrime(p))
            {
                p += 2 * n;
            }
            return p;
        }

        static public bool IsPrime(int x)
        {
            if (x < 2)
                return false;
            for (int i = 2; i * i <= x; i++)
                if (x % i == 0)
                    return false;
            return true;
        }


    }
}
