﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public class RLWE
    {
        static int muchLessMeasure;
        int _n;
        int _q;
        public PolynomialModulo s;
        public PolynomialModulo a;
        public PolynomialModulo b;
        

        public RLWE(int n, int q)
        {
            _n = n;
            _q = q;
            muchLessMeasure = (int)Math.Sqrt(_q / _n / 12);
            if (muchLessMeasure == 0) muchLessMeasure = 1;
            GeneratePrivateKey();
            GeneratePublicKey();
        }


        public string GetPublicKey()
        {
            if (_n > 8)
            {
                return "( a=[" + a.GetCoefficientsString() + "]\n  b=[" + b.GetCoefficientsString() + "] )";
            }
            return "( a = " + a.ToString() + "\n  b = " + b.ToString() + " )";
        }


        public string GetPrivateKey()
        {
            if (_n > 8)
            {
                return "( s=[" + s.GetCoefficientsString() + "] )";
            }
            return "( s = " + s.ToString() + " )";
        }


        private void GeneratePrivateKey()
        {
            s = GetSmallRandomPoly();
        }


        public void GeneratePublicKey()
        {
            PolynomialModulo e = GetSmallRandomPoly();
            a = PolynomialModulo.RandomPolynomial(_n - 1, _q - 1, _q, _n);
            b = a * s + e;
        }


        public void Encrypt(byte[] message, out PolynomialModulo u, out PolynomialModulo v)
        {
            PolynomialModulo z = new PolynomialModulo(message, _q, _n);
            PolynomialModulo e1 = GetSmallRandomPoly();
            PolynomialModulo e2 = GetSmallRandomPoly();
            PolynomialModulo r = GetSmallRandomPoly();
            u = a * r + e1;
            v = b * r + e2 + (_q / 2) * z;
        }


        public byte[] Deencrypt(PolynomialModulo u, PolynomialModulo v)
        {
            byte[] message = new byte[_n];

            PolynomialModulo poly = v - u * s;
            for(int i=0;i<_n && i <= poly.Degree; i++)
            {
                if (poly[i] > _q / 4 && poly[i] < _q / 4 * 3)
                    message[i] = 1;
            }
            return message;
        }


        public void Encrypt(byte[][] message, out PolynomialModulo[] u, out PolynomialModulo[] v)
        {
            u = new PolynomialModulo[message.Length];
            v = new PolynomialModulo[message.Length];
            for(int i = 0; i < message.Length; i++)
            {
                Encrypt(message[i], out u[i], out v[i]);
            }
        }


        public byte[][] Deencrypt(PolynomialModulo[] u, PolynomialModulo[] v)
        {
            if (u.Length != v.Length)
                throw new ArgumentException("Tablice różnych wielkości");
            byte[][] message = new byte[u.Length][];
            for (int i = 0; i < message.Length; i++)
            {
                message[i] = Deencrypt(u[i], v[i]);
            }
            return message;
        }


        private PolynomialModulo GetSmallRandomPoly()
        {
            return PolynomialModulo.RandomPolynomial(_n - 1, muchLessMeasure, _q, _n);
        }
        
    }
}
