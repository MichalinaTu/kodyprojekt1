﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public class RS
    {
        int _q;
        int _n;
        int _alpha;
        int _alpha_odw;
        int _b = 1;
        int _delta;
        PolynomialModulo g;


        public PolynomialModulo GeneratingPolynomial { get { return g; } }
        public int alpha { get { return _alpha; } }
        public int k { get { return _n + 1 - _delta; } }
        public int n { get { return _n; } }
        int[][] _combinations;


        public RS(int q, int delta)
        {
            _q = q;
            _n = q - 1;
            _delta = delta;
            CalculateAlpha();
            GenerateGenaratingPolynomian();
            _combinations = Combinations(_n, k);
        }


        private void CalculateAlpha()
        {
            for (int i = 2; i < _q; i++)
            {
                int x = i;
                int x_pop = i;
                int deg = 1;

                while (x != 1)
                {
                    x_pop = x;
                    x = (x * i) % _q;
                    deg++;
                }
                if (deg == _q - 1)
                {
                    _alpha_odw = x_pop;
                    _alpha = i;
                    return;
                }
            }
        }


        private void GenerateGenaratingPolynomian()
        {
            g = new PolynomialModulo(new int[] { -(int)Math.Pow(_alpha, _b), 1 }, _q, _n);
            for (int i = 1; i <= _delta - 2; i++)
            {
                g = g * new PolynomialModulo(new int[] { -(int)Math.Pow(_alpha, _b + i), 1 }, _q, _n);
            }
        }


        public int[][] Encrypt(int[][] message)
        {
            int[][] vectors = new int[message.Length][];
            for (int i = 0; i < message.Length; i++)
                vectors[i] = Encrypt(message[i]);
            return vectors;
        }


        public int[][] Deencrypt(int[][] message)
        {
            int[][] vectors = new int[message.Length][];
            for (int i = 0; i < message.Length; i++)
                vectors[i] = DeencryptWithMistakes(message[i]);
            return vectors;
        }


        public int[] Encrypt(int[] message)
        {
            PolynomialModulo polynomial = new PolynomialModulo(message, _q, _n);
            int[] vector = new int[_n];
            int alfi = 1;
            for (int i = 0; i < _n; i++)
            {
                vector[i] = polynomial.ValueModulo(alfi);
                alfi = (alfi * _alpha) % _q;
            }
            return vector;
        }


        public int[] DeencryptWithoutMistakes(int[] incomingMessage)
        {
            PolynomialModulo c = new PolynomialModulo(incomingMessage, _q, _n);
            int[] vector = new int[k];
            int alfi = 1;
            for (int i = 0; i < k; i++)
            {
                vector[i] = (_q - c.ValueModulo(alfi)) % _q;
                alfi = (alfi * _alpha_odw) % _q;
            }
            return vector;
        }


        public int[] DeencryptWithMistakes(int[] incomingMessage)
        {
            int[][] matrix = new int[_n][];
            int alfi = 1;
            for (int i = 0; i < _n; i++)
            {
                int alfk = 1;
                matrix[i] = new int[k];
                for (int j = 0; j < k; j++)
                {
                    matrix[i][j] = alfk;
                    alfk = (alfk * alfi) % _q;
                }
                alfi = (alfi * _alpha) % _q;
            }

            int[][] resolves = new int[_combinations.Length][];
            int[] freq = new int[resolves.Length];
            int MostComm = 0;
            for (int i = 0; i < _combinations.Length; i++)
            {
                EquationsSystem equationsSystem = new EquationsSystem(incomingMessage, matrix, _q, k, _combinations[i]);
                resolves[i] = equationsSystem.Resolve();
                for (int j = 0; j < i; j++)
                {
                    if (isEqual(resolves[i], resolves[j]))
                    {
                        freq[j]++;
                        if (freq[MostComm] < freq[j])
                        {
                            MostComm = j;
                        }
                        if (freq[MostComm] > resolves.Length / 2)
                            return resolves[MostComm];
                        break;
                    }
                }
            }
            return resolves[MostComm];
        }


        public static int[][] Combinations(int n, int k)
        {
            List<List<int>> combs = combinations(0, n, k);
            int[][] combins = new int[combs.Count][];
            for (int i = 0; i < combs.Count; i++)
            {
                combins[i] = combs[i].ToArray();
            }
            return combins;
        }


        private static List<List<int>> combinations(int n0, int n, int k)
        {
            List<List<int>> combs = new List<List<int>>();
            if (k == 1)
            {
                for (int i = n0; i < n; i++)
                {
                    List<int> combin = new List<int>();
                    combin.Add(i);
                    combs.Add(combin);
                }
                return combs;
            }
            for (int i = n0 + k - 1; i < n; i++)
            {
                List<List<int>> combins = combinations(n0, i, k - 1);
                foreach (var c in combins)
                {
                    List<int> list = new List<int>();
                    list.AddRange(c);
                    list.Add(i);
                    combs.Add(list);
                }
            }
            return combs;
        }


        private bool isEqual(int[] a, int[] b)
        {
            if (a.Length != b.Length)
                return false;
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] != b[i])
                    return false;
            }
            return true;
        }
    }
}
