﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RLWEiRS
{
    public partial class Form1 : Form
    {
        //RLWE
        int k = 4;
        int n = 16;
        int RLWEq = 97;
        RLWE RLWEcryptosystem;
        PolynomialModulo[] u, v;

        //RS
        int RSq = 5;
        int delta = 3;
        RS RScryptosystem;


        public Form1()
        {
            InitializeComponent();
        }

        /////////////////////// RLWE //////////////////////

        private void button_insert_n_Click(object sender, EventArgs e)
        {
            k = (int)numericUpDown_k.Value;
            n = (int)Math.Pow(2, k);
            RLWEq = Prime.GetLowerPrimeEqual1Mod2n(n);
            label_n.Text = "n=2^" + k + "=" + n;
            label_q.Text = "q=" + RLWEq;
            RLWEcryptosystem = null;
            label_privateKey.Text = "( )";
            label_publicKey.Text = "( : )";
        }


        private void button_encrypt_Click(object sender, EventArgs e)
        {
            if (RLWEcryptosystem != null)
            {
                string text = StringHelper.NormaliseTextLength(richTextBox_source.Text, n);
                byte[][] message = StringHelper.GetBinarySequences(text, n);
                PolynomialModulo[] u, v;
                RLWEcryptosystem.Encrypt(message, out u, out v);
                this.u = u; this.v = v;

                string s = "";
                for (int i = 0; i < u.Length; i++)
                {
                    s += "u=" + u[i].GetCoefficientsString() + "\n" + "v=" + v[i].GetCoefficientsString() + "\n";
                }
                richTextBox_dest.Text = s;
            }

        }


        private void button_generate_private_key_Click(object sender, EventArgs e)
        {
            RLWEcryptosystem = new RLWE(n, RLWEq);
            label_privateKey.Text = RLWEcryptosystem.GetPrivateKey();
            label_publicKey.Text = "( : )";
        }


        private void button_generate_public_key_Click(object sender, EventArgs e)
        {
            if (RLWEcryptosystem != null)
            {
                RLWEcryptosystem.GeneratePublicKey();
                label_publicKey.Text = RLWEcryptosystem.GetPublicKey();
            }
        }


        private void button_decrypt_Click(object sender, EventArgs e)
        {
            if (RLWEcryptosystem != null)
            {
                byte[][] message = RLWEcryptosystem.Deencrypt(u, v);
                richTextBox_dest.Text = StringHelper.GetText(message);
            }
        }

        /////////////////////// RS //////////////////////

        private void button_insert_q_Click(object sender, EventArgs e)
        {
            RSq = (int)numericUpDown_RSq.Value;
            while (!Prime.IsPrime(RSq))
            {
                RSq++;
            }
            numericUpDown_RSq.Value = (decimal)RSq;
            label_qRS.Text = "q=" + RSq;
            refreshRS();
        }


        private void refreshRS()
        {
            RScryptosystem = null;
            label_alfa.Text = "alfa=";
            label_gen_poly.Text = "wielomian generujący: ( )";
        }


        private void button_insert_delta_Click(object sender, EventArgs e)
        {
            delta = (int)numericUpDown_delta.Value;
            label_delta.Text = "delta=" + delta;
            refreshRS();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            RScryptosystem = new RS(RSq, delta);
            label_alfa.Text = "alfa="+RScryptosystem.alpha;
            label_gen_poly.Text = "wielomian generujący: " + RScryptosystem.GeneratingPolynomial;
            if(RScryptosystem.GeneratingPolynomial.Degree>10)
                label_gen_poly.Text = "wielomian generujący: " + RScryptosystem.GeneratingPolynomial.GetCoefficientsString();
        }


        private void button_RSdeencrypt_Click(object sender, EventArgs e)
        {
            if (RScryptosystem != null)
            {
                string text = richTextBox_dest.Text;
                int[][] message = StringHelper.GetSequences(text, RScryptosystem.n);
                int[][] result = RScryptosystem.Deencrypt(message);
                richTextBox_dest.Text = StringHelper.GetText(result);
            }
        }


        private void button_RSencrypt_Click(object sender, EventArgs e)
        {
            if (RScryptosystem != null)
            {
                string text = StringHelper.CleanTextToUpperLetters(richTextBox_source.Text);
                richTextBox_source.Text = text;
                int[][] message = StringHelper.GetSequences(text, RScryptosystem.k);
                int[][] result = RScryptosystem.Encrypt(message);
                richTextBox_dest.Text = StringHelper.GetText(result);
            }
        }

        
    }
}
