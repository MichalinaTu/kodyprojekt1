﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RLWEiRS
{
    public class EquationsSystem
    {
        public int[][] matrix;

        public int[] b;

        private int n;
        private int q;
        private int[] inverse;

        public EquationsSystem() { }

        public EquationsSystem(int[] b, int[][] matrix, int q)
        {
            this.q = q;
            n = b.Length;
            this.b = CopyMod(b);
            if (matrix.Length != n)
                throw new ArgumentException();
            this.matrix = new int[n][];
            for(int i = 0; i < n; i++)
            {
                if (matrix[i].Length != n)
                    throw new ArgumentException();
                this.matrix[i] = CopyMod(matrix[i]);
            }
            CalculateInverseElements();
        }


        public EquationsSystem(int[] b, int[][] matrix, int q, int n, int[] combination)
        {
            this.q = q;
            this.n = n;
            this.b = CopyMod(b,combination);
            this.matrix = new int[n][];
            for (int i = 0; i < n; i++)
            {
                this.matrix[i] = CopyMod(matrix[combination[i]]);
            }
            CalculateInverseElements();
        }


        private void CalculateInverseElements()
        {
            inverse = new int[q + 1];
            for (int i = 1; i <= q; i++)
            {
                if (inverse[i] == 0)
                {
                    for (int j = 1; j <= q; j++)
                    {
                        if (inverse[j] == 0)
                        {
                            if (i * j % q == 1)
                            {
                                inverse[i] = j;
                                inverse[j] = i;
                                break;
                            }
                        }
                    }
                }
            }
        }


        public int[] Resolve()
        {
            Gauss();
            return b;
        }


        public void Gauss()
        {
            for(int i = 0; i < n; i++)
            {
                MakeOne(i);
                MakeZeros(i);
            }
        }


        public void MakeOne(int i)
        {
            int k = matrix[i][i];
            for (int j = 0; j < n; j++)
            {
                matrix[i][j] = (matrix[i][j] * inverse[k])%q;
            }
            b[i] = (b[i] * inverse[k])%q;
        }


        public void MakeZero(int i,int j)
        {
            int k = matrix[j][i];
            for (int l = 0; l < n; l++)
            {
                matrix[j][l] = (matrix[j][l] - matrix[i][l] * k)%q;
                if (matrix[j][l] < 0) matrix[j][l] += q;
            }
            b[j] = (b[j] - b[i] * k) % q;
            if (b[j] < 0) b[j] += q;
        }


        public void MakeZeros(int i)
        {
            for (int l = 0; l < i; l++)
            {
                MakeZero(i, l);
            }
            for (int l = i + 1; l < n; l++)
            {
                MakeZero(i, l);
            }
        }


        private int[] CopyMod(int[] tab)
        {
            int[] copy = new int[tab.Length];
            for (int i = 0; i < tab.Length; i++)
            {
                copy[i] = tab[i] % q;
                if (copy[i] < 0) copy[i] += q;
            }
            return copy;
        }


        private static int[] CopyMod(int[] tab, int[] combination)
        {
            int[] copy = new int[combination.Length];
            for (int i = 0; i < combination.Length; i++)
                copy[i] = tab[combination[i]];
            return copy;
        }
    }
}
